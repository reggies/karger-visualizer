module Geometry
       ( euclideanDistance
       , middle
       , normalize
       , minus
       , plus
       , scale
       , neg
       , perp
       , near) where

import Types

pmap f (x, y) = (f x, f y)
pzip f (a, b) (c, d) = (f a c, f b d)

euclideanDistance :: Point -> Point -> Float
euclideanDistance (x0,y0) (x1,y1) = sqrt ((x0-x1)^^2 + (y0-y1)^^2)

near :: Point -> Point -> Bool
near x y = euclideanDistance x y < 1e-3

scale :: Float -> Point -> Point
scale k = pmap (*k)

neg, perp, normalize :: Point -> Point
neg = pmap negate
perp (x, y) = (y, -x)
normalize (x, y) = let m = sqrt (x^^2 + y^^2) in (x / m, y / m)

middle, plus, minus :: Point -> Point -> Point
minus = pzip (-)
plus = pzip (+)
middle a = scale (0.5) . plus a