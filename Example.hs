module Example 
       (loadExampleGraph
       ) where

import ContractableGraph
import Auxiliary
import Frame
import Types
import qualified Data.Map as Map

example = Map.fromList [
  (1, ((-0.25, -0.5), [2,3,4,5])),
  (2, ((0.25, -0.5), [3,4,5,6])),
  (3, ((-0.5, -0.25), [4,7,8])),
  (4, ((-0.25, -0.25), [5,7,8,9])),
  (5, ((0.25, -0.25), [6,8,9,10])),
  (6, ((0.5, -0.25), [9,10])),
  (7, ((-0.5, 0.25), [8,11])),
  (8, ((-0.25, 0.25), [9,11,12])),
  (9, ((0.25, 0.25), [10,11,12])),
  (10, ((0.5, 0.25), [12])),
  (11, ((-0.25, 0.5), [12])),
  (12, ((0.25, 0.5), []))
  ]

loadExampleGraph :: FrameState -> FrameState
loadExampleGraph frame = let
  edges = concat . map (\ (v, (_, xs)) -> [mkEdge v u | u <- xs]) . Map.toList $ example
  in frame {    
    vertexPoint = Map.map fst example,
    graph = buildGr (Map.keys example) edges
    }