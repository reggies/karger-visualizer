module Types 
       ( Vertex
       , Edge
       , Set
       , Color
       , Point
       ) where

import qualified Data.Set as Set
import qualified Graphics.Rendering.OpenGL.GL as GL
import qualified Data.Graph as Graph
import qualified Data.Map as Map

type Vertex = Graph.Vertex
type Graph = Graph.Graph
type Edge = Graph.Edge
type Set = Set.Set
type Color = GL.Color4 Float
type Point = (Float, Float)
