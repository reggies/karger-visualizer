module Draw
       ( draw
       ) where

import Data.Traversable
import Data.Foldable
import Data.Maybe
import Data.Ord
import Data.Map as Map
import Data.List as L
import Control.Monad.Reader
import Graphics.Rendering.OpenGL.GL hiding (Color, Vertex)
import Graphics.UI.SDL hiding (Color)

import ContractableGraph
import Frame
import Types
import Geometry as G
import Auxiliary

segments = 20

vertexRadius = 0.05

parallelEdgeGap length = length * 0.2

black = mkColor 0 0 0
white = mkColor 1 1 1

findVertexPoint v = fromJust . Map.lookup v . vertexPoint

curveSegments :: Point -> Point -> Point -> [Point]
curveSegments a b c = do
  t <- [0..segments-1]
  let x = fromIntegral t / fromIntegral (segments - 1)
  let y = 1 - x
  return $ foldl1' plus [G.scale (x^^2) a, G.scale (2*x*y) b, G.scale (y^^2) c]

curve :: Color -> Point -> Point -> Point -> IO ()
curve color' a b c = renderPrimitive LineStrip $ do
  color color'
  traverse_ (vertex . uncurry Vertex2) $ curveSegments a b c

curveMiddlePoints :: Point -> Point -> Int -> [Point]
curveMiddlePoints a b n = let
  mid = middle a b
  clearance = parallelEdgeGap ( euclideanDistance a b )
  layoutStep = G.scale clearance . perp . G.normalize $ minus mid a
  in do
    t <- [1..n]
    let alignment = fromIntegral t - fromIntegral (n + 1) / 2
    return . plus mid . G.scale alignment $ layoutStep

drawParallelEdge [] = return ()
drawParallelEdge (x:xs) = do
  color' <- asks $ Map.findWithDefault black x . edgeColor
  sourcePoint <- asks . findVertexPoint . source $ x
  targetPoint <- asks . findVertexPoint . target $ x
  let curve' middlePoint = curve color' sourcePoint middlePoint targetPoint
  liftIO . traverse_ curve' . curveMiddlePoints sourcePoint targetPoint . length $ x:xs
  
drawEdges = ask >>= traverse_ drawParallelEdge . group . sort . edges . graph

circleSegments :: Float -> [Point]
circleSegments r = let
  step = 2 * pi / fromIntegral segments
  in do
    t <- [0..segments]
    let x = r * cos (fromIntegral t * step)
    let y = r * sin (fromIntegral t * step)
    return (mkPoint x y)

circle :: Color -> Point -> Float -> IO ()
circle c (x,y) r = preservingMatrix $ do
  translate $ Vector3 x y 0
  renderPrimitive Polygon $ do
    color c
    traverse_ (vertex . uncurry Vertex2) . circleSegments $ r
  
drawVertex vertex = do
  pos <- asks . findVertexPoint $ vertex
  color <- asks $ Map.findWithDefault white vertex . vertexColor
  liftIO $ do
    circle black pos vertexRadius
    circle color pos (vertexRadius * 9 / 10)

drawVertices = ask >>= traverse_ drawVertex . vertices . graph

draw :: FrameState -> IO ()
draw frame = do
  loadIdentity
  clearColor $= white
  lineWidth $= 2
  clear [ColorBuffer]
  runReaderT (drawEdges >> drawVertices) frame
  surface <- getVideoSurface
  Graphics.UI.SDL.flip surface
  glSwapBuffers