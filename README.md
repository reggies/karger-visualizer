Karger's mincut algorithm visualization written in Haskell.

Requirements
============
Tested with The Glorious Glasgow Haskell Compilation System, version 7.4.2.

* mtl >= 2.0
* SDL >= 0.6.4

How to run
==========

Build using GNU make
--------------------
Build:
~~~
  $ cabal install mtl sdl
  $ make
~~~

Run:
~~~
  $ ./Main
~~~

MIT License
===========
See LICENSE for details.
