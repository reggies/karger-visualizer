all: $(wildcard *.hs)
	ghc --make -O2 Main.hs -o Main

clean:
	rm -f Main *.o *.hi