module Frame
  ( FrameState (..)
  , Frame
  , runFrame
  , emptyFrame
  ) where

import Data.Foldable as F
import Data.Maybe
import qualified Data.Set as Set
import Data.Map as Map
import Control.Monad.State
import Control.Monad
import ContractableGraph as G
import Types
import Geometry
import Auxiliary

data FrameState = FrameState {
  nextFrame :: Frame (),
  graph :: ContractableGraph,
  vertexColor :: Map Vertex Color,
  vertexPoint :: Map Vertex Point,
  originalVertexPoint :: Map Vertex Point,
  edgeColor :: Map Edge Color
  }

type Frame = StateT FrameState IO

-- , frames
delayAfterPickEdge = 10
delayAfterContractEdge = 10
delayAfterRestoreGraph = 35
delayBeforeRestoreGraph = 35

contractedColor :: Color
contractedColor = mkColor 1 0 0

accelerated :: Point -> Point -> Point
accelerated from to = plus from . scale 0.15 . minus to $ from

jump :: Frame () -> Frame ()
jump to = modify $ \st -> st {nextFrame = to}

wait :: Integer -> Frame () -> Frame ()
wait 0 next = next
wait n next = jump . wait (pred n) $ next

loop :: Frame Bool -> Frame () -> Frame ()
loop condition next = do
  condition' <- condition
  when condition' . jump $ next

runFrame :: Frame ()
runFrame = get >>= nextFrame

emptyFrame :: FrameState
emptyFrame = FrameState {
  nextFrame = start,
  graph = G.empty,
  vertexColor = Map.empty,
  vertexPoint = Map.empty,
  originalVertexPoint = Map.empty,
  edgeColor = Map.empty
  }

start = do
  vertexPoints <- gets vertexPoint
  modify $ \st -> st {
    originalVertexPoint = vertexPoints,
    vertexColor = Map.empty,
    edgeColor = Map.empty
    }
  jump initialCondition

end = jump start

initialCondition = do
  graph' <- gets graph
  case vertices graph' of
    [a,b] -> do
      let Just cut = findParentSet a graph'
      F.traverse_ (paintVertex contractedColor) cut
      jump . wait delayBeforeRestoreGraph $ restoreGraph
    _ -> do
      edge <- pickEdge
      paintEdge contractedColor edge
      jump . wait delayAfterPickEdge $ contractGraph edge
      
pickEdge :: Frame Edge
pickEdge = gets graph >>= liftIO . randomEdge

paintVertex :: Color -> Vertex -> Frame ()
paintVertex c v = do
  vertexColor' <- gets (insert v c . vertexColor)
  modify $ \st -> st {vertexColor = vertexColor'}

paintEdge :: Color -> Edge -> Frame ()
paintEdge c e = do
  edgeColor' <- gets (insert e c . edgeColor)
  modify $ \st -> st {edgeColor = edgeColor'}

move :: Vertex -> Point -> Frame ()
move v p = do
  vertexPoint' <- gets (insert v p . vertexPoint)
  modify $ \st -> st {vertexPoint = vertexPoint'}

contractGraph :: Edge -> Frame ()
contractGraph edge = let
  u = source edge
  v = target edge
  finally = do
    graph' <- gets (merge edge . graph)
    modify $ \st -> st { graph = graph' }
    jump . wait delayAfterContractEdge $ initialCondition  
  desire x = return . near x
  animate x y = do
    move u . accelerated x $ y
    move v . accelerated y $ x
  animateLoop = do
    Just x <- gets $ Map.lookup u . vertexPoint
    Just y <- gets $ Map.lookup v . vertexPoint
    loop (animate x y >> desire x y) finally
  in jump animateLoop

restoreGraph :: Frame ()
restoreGraph = do
  graph' <- gets (G.split . graph)
  vertexPoint' <- gets originalVertexPoint
  modify $ \st -> st { graph = graph'
                     , vertexPoint = vertexPoint'
                     , edgeColor = Map.empty
                     }
  jump . wait delayAfterRestoreGraph $ end