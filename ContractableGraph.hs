module ContractableGraph
       ( ContractableGraph ()
       , randomEdge
       , edges
       , vertices
       , merge
       , split
       , buildGr
       , findParentSet
       , empty
       ) where

import Control.Monad
import qualified System.Random as Random
import Data.List
import qualified Data.Graph as Graph
import qualified Data.Set as Set
import Data.Maybe
import Types
import Auxiliary
import qualified Data.Map as M

data ContractableGraph = ContractableGraph { 
  edgeList :: [Edge],
  byLeader :: M.Map Vertex (Set Vertex),
  byVertex :: M.Map Vertex Vertex
  }

empty :: ContractableGraph
empty = buildGr [] []

leader :: Vertex -> ContractableGraph -> Maybe Vertex
leader v = M.lookup v . byVertex

findParentSet :: Vertex -> ContractableGraph -> Maybe (Set Vertex)
findParentSet v g = (M.lookup v (byVertex g)) >>= (flip M.lookup (byLeader g))

edges :: ContractableGraph -> [Edge]
edges g = catMaybes . map leaders . edgeList $ g
  where leaders edge = let
          Just u_leader = leader (source edge) g
          Just v_leader = leader (target edge) g
          in if u_leader == v_leader
             then Nothing
             else Just $ mkEdge u_leader v_leader

randomEdge :: ContractableGraph -> IO Edge
randomEdge g = do
  let edges' = edges g
  n <- Random.randomIO
  return $ edges' !! (n `mod` (length edges'))

vertices :: ContractableGraph -> [Vertex]
vertices = M.keys . byLeader

merge :: Edge -> ContractableGraph -> ContractableGraph
merge edge g = let
  (u, v) = (source edge, target edge)
  Just u_leader = leader u g
  Just v_leader = leader v g
  (looser, winner) = (min u_leader v_leader, max u_leader v_leader)
  Just u_set = M.lookup u_leader (byLeader g)
  Just v_set = M.lookup v_leader (byLeader g)
  merged = Set.union u_set v_set  
  in g { byLeader = M.insert winner merged . M.delete looser . byLeader $ g
       , byVertex = Set.foldl' (\m v -> M.insert v winner m) (byVertex g) merged
       }

buildMap f = M.fromList . map (\v -> (v, f v))

split :: ContractableGraph -> ContractableGraph
split g = let 
  vs = M.keys . byVertex $ g
  in g {
    byLeader = buildMap Set.singleton vs,
    byVertex = buildMap id vs
    }

buildGr :: [Vertex] -> [Edge] -> ContractableGraph
buildGr vs es = ContractableGraph {
  byLeader = buildMap Set.singleton vs,
  byVertex = buildMap id vs,
  edgeList = es
  }