module Auxiliary
       ( mkPoint
       , mkColor
       , mkEdge
       , source
       , target
       , undirect )
       where

import qualified Graphics.Rendering.OpenGL.GL as GL
import Types

mkEdge :: Vertex -> Vertex -> Edge
mkEdge u v = (source (u, v), target (u, v))

mkColor :: Float -> Float -> Float -> Color
mkColor r g b = GL.Color4 r g b 1.0

mkPoint :: Float -> Float -> Point
mkPoint = (,)

source :: Edge -> Vertex
source = uncurry min

target :: Edge -> Vertex
target = uncurry max

undirect :: Edge -> Edge
undirect e = (source e, target e)