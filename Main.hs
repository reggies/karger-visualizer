module Main where

import qualified Data.Map as Map
import Control.Monad.State
import Graphics.UI.SDL
import ContractableGraph
import Frame
import Draw
import Auxiliary
import Example

frameRate = 24
screenWidth = 600
screenHeight = 600
screenBits = 24

shouldBreak :: IO Bool
shouldBreak = do
  ev <- pollEvent
  case ev of
    Quit -> return True
    NoEvent -> return False
    KeyDown (Keysym SDLK_ESCAPE _ _) -> return True
    _ -> shouldBreak

loop fpstimer = do
  runFrame
  get >>= liftIO . draw
  quit <- liftIO shouldBreak
  unless quit $ do
    now <- liftIO getTicks
    let ticksToFrame = fpstimer + ticksPerFrame
    when (now < ticksToFrame) $ do
      liftIO $ delay $ ticksToFrame - now
    loop now
    where
      ticksPerFrame = 1000 `div` frameRate

initFrame = return $ loadExampleGraph emptyFrame

runLoop = do
  fr <- initFrame
  now <- getTicks
  evalStateT (loop now) fr

visualize = withInit [InitVideo] $ do
  setVideoMode screenWidth screenHeight screenBits [OpenGL, HWSurface]
  runLoop

main :: IO ()
main = visualize